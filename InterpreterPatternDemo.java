package InterpreterPattern;

/**
 *
 * @author henok
 */
public class InterpreterPatternDemo {

   //Rule: Robert and John are male
   public static Expression getMaleExpression(){
      Expression robert = new TerminalExpression("Robert");
      Expression john = new TerminalExpression("John");
      return new OrExpression(robert, john);		
   }

   //Rule: Julie is a married women
   public static Expression getMarriedWomanExpression(){
      Expression aster = new TerminalExpression("aster");
      Expression married = new TerminalExpression("Married");
      return new AndExpression(aster, married);		
   }

   public static void main(String[] args) {
      Expression isMale = getMaleExpression();
      Expression isMarriedWoman = getMarriedWomanExpression();

      System.out.println("John is male? " + isMale.interpret("John"));
      System.out.println("aster is a married women? " + isMarriedWoman.interpret("Married Robert"));
   }
}

 

 
